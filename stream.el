;;; -*- lexical-binding: t -*-
;; A stream is either the symbol 'stream-nil representing empty stream or a
;; pair whose first element is the head of the stream and the second element
;; is a thunk where a thunk is a function that takes no paramenters and when
;; run produces a stream.

(require 'cl-lib)
(require 'dash)
(require 'dash-functional)

;; Calc version of arithmetic operations
(defun -C (a b)
  (let ((x (or (and (numberp a) (number-to-string a)) a))
	(y (or (and (numberp b) (number-to-string b)) b)))
    (calc-eval (format "%s - %s" x y))))

(defun +C (a b)
  (let ((x (or (and (numberp a) (number-to-string a)) a))
	(y (or (and (numberp b) (number-to-string b)) b)))
    (calc-eval (format "%s + %s" x y))))

(defun =C (a b)
  (let ((x (or (and (numberp a) (number-to-string a)) a))
	(y (or (and (numberp b) (number-to-string b)) b)))
    (let ((result (calc-eval (format "%s == %s" x y))))
      (pcase result
	("1" t)
	("0" nil)))))

(defun >C (a b)
  (let ((x (or (and (numberp a) (number-to-string a)) a))
	(y (or (and (numberp b) (number-to-string b)) b)))
    (let ((result (calc-eval (format "%s > %s" x y))))
      (pcase result
	("1" t)
	("0" nil)))))

(defun >=C (a b)
  (let ((x (or (and (numberp a) (number-to-string a)) a))
	(y (or (and (numberp b) (number-to-string b)) b)))
    (let ((result (calc-eval (format "%s >= %s" x y))))
      (pcase result
	("1" t)
	("0" nil)))))

;; Stream implementation begins.
(defmacro stream-cons (x s)
  "Append element X to stream S and return a new stream."
  `(cons ,x (lambda () ,s)))

(defun stream-car (s)
  "Return the leading element of the stream S."
  (car s))

(defun stream-cdr (s)
  "Return the rest of the stream sans leading element of stream S."
  (funcall (cdr s)))

(defun list-to-stream (l)
  "Convert list L to stream."
  (defun list-thunk (xs)
    "Returns a thunk i.e. a lambda function that taken nsero parameters and when executed returns a stream.
N.B. Calling this function does not return a stream but a thunk. This thunk has to be called to get a stream."
    (lambda ()
      (if (equal nil xs)
	  'stream-nil
	(let ((x (car xs))
	      (s (cdr xs)))
	  (stream-cons x (funcall (list-thunk s)))))))
  (funcall (list-thunk l)))

(defun stream-to-list (xs)
  "Convert stream to list."
  (if (equal 'stream-nil xs)
      nil
    (let ((x (stream-car xs))
	  (s (stream-cdr xs)))
      (cons x (stream-to-list s)))))

(defun stream-map (f xs)
  "Apply function f to each element of stream S and return a new resulting stream."
  (if (equal 'stream-nil xs)
      'stream-nil
    (let ((x (stream-car xs))
	  (s (stream-cdr xs)))
      (stream-cons (funcall f x) (stream-map f s)))))

(defun stream-take (n xs)
  "Return the first N elements of stream S and return that as a stream."
  (cond ((>C 0 n) (error "cannot take -ve number of elements from stream."))
	((=C 0 n) 'stream-nil)
	((equal 'stream-nil xs) 'stream-nil)
	((let ((x (stream-car xs))
	       (s (stream-cdr xs)))
	   (stream-cons x (stream-take (- n 1) s))))))

(defun stream-drop (n xs)
  "Drop the first N elements of stream S and return rest of the stream.
N.B. This is tail call optimized so that tail position recursice calls of stream-drop does not blow the stack."
  (defun stream-drop-step (i ys)
    (cond ((>C 0 i) (error "cannot drop -ve number of elements from stream."))
	  ((=C 0 i) ys)
	  ((equal 'stream-nil ys) 'stream-nil)
	  ((lambda () (stream-drop-step (-C i 1) (stream-cdr ys))))))
  (let ((result (stream-drop-step n xs)))
    (while (functionp result)
      (setq result (funcall result)))
    result))

(defun stream-repeat (a)
  "Produces an infinite stream of A's"
  (stream-cons a (stream-repeat a)))

(defun stream-range (n &rest m)
  "Generate a stream on numbers from N to M - 1."
  (cond ((and (not (equal nil m)) (>=C n m)) 'stream-nil)
	((stream-cons n (stream-range (+C n 1) m)))))

(defun stream-append (xs ys)
  "Append to end of stream XS another stream YS and return the combined stream."
  (if (equal 'stream-nil xs)
      ys
    (let ((x (stream-car xs))
	  (s (stream-cdr xs)))
      (stream-cons x (stream-append s ys)))))

(defun stream-foldr (f z xs)
  "Take the function F, the unit value Z and stream XS and fold the stream from right.
N.B. F takes an element of the stream XS and an intermediate folded value - which is of the same type as Z - and produce a new folded value of the type Z."
  (if (equal 'stream-nil xs)
      z
    (let ((x (stream-car xs))
	  (s (stream-cdr xs)))
      (funcall f x (stream-foldr f z s)))))

(defun stream-foldl (f z xs)
  "Take the function F, the unit value Z and stream XS and fold the stream from left.
N.B. F takes an intermediate folded value - which is of the same type as Z - and an element of the stream XS and produce a new folded value of the type Z."
  (if (equal 'stream-nil xs)
      z
    (let ((x (stream-car xs))
	  (s (stream-cdr xs)))
      (stream-foldl f (funcall f z x) s))))

(defun stream-pure (a)
  "Create a stream of one element A."
  (stream-cons a 'stream-nil))

(defun stream-filter (p xs)
  "Take the stream XS and keep only those elements which satisfies the predicate P to produce a new stream."
  (stream-foldl
   #'stream-append
   'stream-nil
   (stream-map
    (lambda (x)
      (if (funcall p x)
	  (stream-pure x)
	'stream-nil))
    xs)))

;; Testing stream
(setq s (list-to-stream '(1 2 3)))
(print s)
(stream-car s)
(stream-cdr (stream-cdr (stream-cdr s)))
(stream-to-list s)
(stream-to-list (list-to-stream '(1 2 3)))
(stream-to-list (stream-map (lambda (x) (* x x)) (list-to-stream '(0 1 2 3 4 5 6 7 8 9))))
(stream-to-list (stream-take 5 (stream-repeat 1)))
(stream-to-list (stream-take 5 (stream-range "1" "3")))
(stream-foldr #'+C 0 (list-to-stream '(0 1 2 3 4 5 6 7 8 9)))
(stream-foldl #'+C 0 (list-to-stream '(0 1 2 3 4 5 6 7 8 9)))
(stream-to-list (stream-filter (lambda (x) (>=C x 5)) (list-to-stream '(0 1 2 3 4 5 6 7 8 9))))
(stream-take 1000 (stream-repeat "1"))
(benchmark-run (stream-drop 10000 (stream-repeat "1")))
(setq debug-on-error t)

;; Fibonacci stream
(defun fibonacci ()
  "Retuns stream of fibonacci numbers."
  (defun fibonacci-step (i x y)
    "Returns a stream of fibonacci numbers starting from 3rd number in the series."
    (let ((ni (+C i 1))
	  (nx (+C x y))
	  (ny x))
      (stream-cons nx (fibonacci-step ni nx ny))))
  (stream-cons "0" (stream-cons "1" (fibonacci-step 1 1 0))))

(benchmark-run (stream-to-list (stream-take 1 (stream-drop 10000 (fibonacci)))))

;; scratchpad
(require 'calc)
(calc-eval "nextprime($1)" nil 101)
