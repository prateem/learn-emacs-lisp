;;; package --- commentary
;;; Commentary:
;;; -*- lexical-binding: t -*-
(require 'dash)
(require 'dash-functional)
;;; code:
(setq lexical-binding t)
(-map (lambda (x) (+ x 1)) '(1 2 3 4 5))
(defun pair (a b) "A B." (lambda (f) (funcall f a b)))
(defun fst (p) "P." (funcall p (lambda (a b) a)))
(defun snd (p) "P." (funcall p (lambda (a b) b)))

(setq person (pair "Prateem" 40))
(fst person)
(snd person)

(message "Prateem")

(defun foo (x)
  "Interactive X."
  (interactive
   (+ x 1)))

(defun mk-rational (n d) "N D." `(,n ,d))
(defun numer (r) "R." (car r))
(defun denom (r) "R." (cadr r))
(setq s (mk-rational 2 3))
(numer s)
(denom s)

(defun factorial-naïve (n)
  "N."
  (if (= n 0)
      1
    (* n (factorial-naïve (- n 1)))))

(factorial-naïve 50)

(functionp (lambda () (display "Prateem")))

(defun *& (a b)
  "A B."
  (let ((x (if (numberp a) (number-to-string a) a))
	(y (if (numberp b) (number-to-string b) b)))
    (calc-eval (format "%s * %s" x y))))

(defun -& (a b)
  "A B."
  (let ((x (if (numberp a) (number-to-string a) a))
	(y (if (numberp b) (number-to-string b) b)))
    (calc-eval (format "%s - %s" x y))))

(defun +& (a b)
  "A B."
  (let ((x (if (numberp a) (number-to-string a) a))
	(y (if (numberp b) (number-to-string b) b)))
    (calc-eval (format "%s + %s" x y))))

(defun =& (a b)
  "A B."
  (let ((x (if (numberp a) (number-to-string a) a))
	(y (if (numberp b) (number-to-string b) b)))
    (equal "1" (calc-eval (format "%s = %s" x y)))))

(*& 3 4)
(*& "3" "4")
(*& "3" 4)
(*& 3 "4")
(calc-eval "2 == 2")
(=& 1 2)
(=& 1 1)

(defun factorial (n)
  "N."
  (defun loop (m k)
    (if (=& m 0)
	(lambda () (funcall k 1))
      (lambda () (loop (-& m 1) (lambda (r) (funcall k (*& m r)))))))
  (let ((f (loop n (lambda (r) (message "%s" r)))))
    (while (functionp f)
      (setq f (funcall f)))
    f))

(factorial 30)

(defun factorial-tco (n)
  "N."
  (defun loop (acc m)
    (if (=& m 0)
	(lambda () acc)
      (lambda () (loop (*& m acc) (-& m 1)))))
  (let ((r (loop 1 n)))
    (while (functionp r)
      (setq r (funcall r)))
    r))

(factorial-tco 10000)

(setq numbers '(1 2 3 4 5 6))
(add-to-list 'numbers 4)
(nth 0 numbers)
(member 10 numbers)

(setq xs '(0 1 2 3 4 5 6 7 8 9))
(dolist (x xs 'done)
  (message (number-to-string x)))

(defun square (n)
  "N."
  (interactive "nn: ")
  (message (number-to-string (* n n))))

(defun greet (name)
  "NAME."
  (interactive "sName: ")
  (message (concat "Hello" " " name)))

(condition-case e
    (* "hello" " " "world")
  ('error (message (format "caught: %s" e))))

(message (format "%s" "%s"))

(setq c "a")
(integerp ?\t)
(elt [1 2 3] 0)
(equal (elt "prateem" 1) ?r)

(setq pi 3.1415)

(defvar pi 3.1415 "Value of constant PI.")

(defun area (r)
  "R."
  (* pi r r))

(-map #'area '(1 2 3))
;;; hello.el ends here
