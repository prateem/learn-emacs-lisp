;;; -*- lexical-binding: t -*-
(require 'cl-lib)
(require 'dash)
(require 'dash-functional)
(require 'generator)

(setq lexical-binding t)

;; Define pair structure
(defun pair (a b)
  "Make pair structure."
  (lambda (f) (funcall f a b)))
(defun fst (p)
  "Select the first element of the pair."
  (funcall p (lambda (a b) a)))
(defun snd (p)
  "Select the second element of the pair."
  (funcall p (lambda (a b) b)))

;; Testing pair structure
(setq person (pair "Prateem" 40))
(fst person)
(snd person)

;; Flip the order of the arguments
(defun flip (f)
  "Return a function with flipped order of arguments to the passed in function."
  (lambda (b a) (funcall f a b)))

;; Identity function
(defun id (a) a)

;; Calc version of arithmetic operations for infinite precision.
(defun =C (x y)
  (let* ((xs (or (and (numberp x) (number-to-string x)) x))
	 (ys (or (and (numberp y) (number-to-string y)) y))
	 (rs (calc-eval (format "%s == %s" xs ys))))
    (pcase rs
      ("1" t)
      ("0" nil)
      (_ (error "Equality check of numbers should return either \"0\" or \"1\".")))))
(defun -C (x y)
  (let* ((xs (or (and (numberp x) (number-to-string x)) x))
	 (ys (or (and (numberp y) (number-to-string y)) y))
	 (rs (calc-eval (format "%s - %s" xs ys))))
    rs))
(defun +C (x y)
  (let* ((xs (or (and (numberp x) (number-to-string x)) x))
	 (ys (or (and (numberp y) (number-to-string y)) y))
	 (rs (calc-eval (format "%s + %s" xs ys))))
    rs))
(defun *C (x y)
  (let* ((xs (or (and (numberp x) (number-to-string x)) x))
	 (ys (or (and (numberp y) (number-to-string y)) y))
	 (rs (calc-eval (format "%s * %s" xs ys))))
    rs))

;; CPS style calc version of arithmetic for infinite precision.
(defun =& (x y k)
  (let* ((xs (or (and (numberp x) (number-to-string x)) x))
	 (ys (or (and (numberp y) (number-to-string y)) y))
	 (rs (calc-eval (format "%s == %s" xs ys))))
    (pcase rs
      ("1" (funcall k t))
      ("0" (funcall k nil)))))
(defun -& (x y k)
  (let* ((xs (or (and (numberp x) (number-to-string x)) x))
	 (ys (or (and (numberp y) (number-to-string y)) y))
	 (rs (calc-eval (format "%s - %s" xs ys))))
    (funcall k rs)))
(defun +& (x y k)
  (let* ((xs (or (and (numberp x) (number-to-string x)) x))
	 (ys (or (and (numberp y) (number-to-string y)) y))
	 (rs (calc-eval (format "%s + %s" xs ys))))
    (funcall k rs)))
(defun *& (x y k)
  (let* ((xs (or (and (numberp x) (number-to-string x)) x))
	 (ys (or (and (numberp y) (number-to-string y)) y))
	 (rs (calc-eval (format "%s * %s" xs ys))))
    (funcall k rs)))

;; Basic factorial implementation 
(defun factorial-basic (n)
  "Basic factorial-basic implementation."
  (if (equal n 0)
      1
    (* n (factorial-basic (- n 1)))))

;; Tail call factorial implementation
(defun factorial-tc (n acc)
  "Tail call implementation of factorial."
  (if (equal n 0)
      acc
    (factorial-tc (- n 1) (* n acc))))

;; Tail call optimized implementation
(defun factorial-tco (n acc)
  "Tail call optimized implementation of factorial function."
  (defun step (i a)
    (lambda () (if (=C i 0)
		   a
		 (lambda () (step (-C i 1) (*C a i))))))
  (let ((result (step n acc)))
    (while (functionp result)
      (setq result (funcall result)))
    result))

;; CPS style factorial implementation
(defun factorial-cps (n k)
  "Factorial function in continuation passing style."
  (=& n 0 (lambda (iszero)
	    (if iszero
		(funcall k 1)
	      (-& n 1 (lambda (nm1)
			(factorial-cps nm1 (lambda (fnm1)
					     (*& n fnm1 k)))))))))

;; Fibonacci
(defun fibonacci-basic (n)
  "Returns the nth fibonacci number."
  (let ((ns (or (and (numberp n) (number-to-string n)) n)))
    (pcase ns
      ("0" 0)
      ("1" 1)
      (_ (+C (fibonacci-basic (-C n 1))
	     (fibonacci-basic (-C n 2)))))))

;; Fibonacci tail call
(defun fibonacci-tc (n)
  (defun loop (i x y)
    (if (equal i n)
	x
      (let ((nx (+ x y))
	    (ny x)
	    (ni (+ i 1)))
	(loop ni nx ny))))
  (pcase n
    (0 0)
    (1 1)
    (_ (loop 1 1 0))))

;; Fibonacci tail call optimized
(defun fibonacci-tco (n)
  "Tail call optimized version of fibonacci."
  (defun loop (i x y)
    (lambda ()
      (if (=C n i)
	  x
	(lambda ()
	  (let ((nx (+C x y))
		(ny x)
		(ni (+C i 1)))
	    (loop ni nx ny))))))
  (let ((ns (or (and (numberp n) (number-to-string n)) n)))
    (pcase ns
      ("0" 0)
      ("1" 1)
      (_ (let ((result (loop "1" "1" "0")))
	   (while (functionp result)
	     (setq result (funcall result)))
	   result)))))

;; Fibonacci CPS style
(defun fibonacci-cps (n k)
  "CPS style implementation of fibonacci."
  (let ((ns (or (and (numberp n) (number-to-string n)) n)))
    (pcase ns
      ("0" (funcall k 0))
      ("1" (funcall k 1))
      (_ (-& ns 1 (lambda (nm1)
		    (fibonacci-cps nm1 (lambda (fnm1)
					 (-& ns 2 (lambda (nm2)
						    (fibonacci-cps nm2 (lambda (fnm2)
									 (+& fnm1 fnm2 k)))))))))))))
;; ;; Fibonacci through generators
;; ;; Does not work. Seems generators have issue yielding from inside lambda functions.
;; (iter-defun fibonacci-gen ()
;;   "Generate fibonacci sequence."
;;   (defun loop (i c p)
;;     (let ((ni (+ i 1))
;; 	  (nc (+ c p))
;; 	  (np c))
;;       (iter-yield nc)
;;       (loop ni nc np)))
;;   (iter-yield 0)
;;   (iter-yield 1)
;;   (loop 1 1 0))

;; (let ((fibs (fibonacci-gen))
;;       (count 100))
;;   (while (>= count 0)
;;     (message "%d. %d" (- 100 count) (iter-next fibs))
;;     (setq count (- count 1))))

;; Lazy streams

;; Testing factorial-basic function.
(setq nums '(0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19))
(-map #'factorial-basic nums)
(-map (apply-partially (flip #'factorial-tc) 1) nums)
(-map (apply-partially (flip #'factorial-cps) #'id) nums)
(-map (apply-partially (flip #'factorial-tco) 1) nums)
(-map (apply-partially (flip #'factorial-tco) 1)
      (-map (lambda (x) (* x 10)) nums))

;; Testing fibonacci
(print (-map #'fibonacci-basic nums))
(print (-map #'fibonacci-tc nums))
(print (-map #'fibonacci-tco nums))
(print (benchmark-run (fibonacci-tco 3000)))


