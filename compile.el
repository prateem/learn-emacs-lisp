;;; compile.el --- Learn about compilation process -*- lexical-binding: nil -*-
;;; Commentary:
;; Learn gccemacs's compilation process
;;; Code:

;; (defun foo (s)
;;   "Show greeting message S."
;;   (message "%s" s))

(defun foo (a b &optional c)
     "A B C."
     (+ a b c))
(byte-compile #'foo)
(disassemble #'foo)
(disassemble '(foo 2 4))

(provide 'compile)

;;; compile.el ends here
