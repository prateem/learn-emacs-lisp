;;; package --- SICP Chapter#1.1: Elements of Programming -*- lexical-binding: t -*-
;;; Commentary:
;;; We will look into expressions, naming,
;;; environment, procedures and procedural abstractions.
;;; `false` is `nil` and `true` is anything non `nil`
;;; generally `t` by convention.
;;; `if` is a special form expression with some differences.
;;; Code:

(require 'dash)
(require 'dash-functional)
(require 'ert)

(defun square (x)
  "X."
  (* x x))

(defun sum-of-squares (x y)
  "X Y."
  (+ (square x) (square y)))

(defun abs (x)
  "X."
  (cond ((> x 0) x)
	((= x 0) 0)
	((< x 0) (* -1 x))))

;; (message (format "%d" (abs -2)))

;; (if (> 1 2) (message "1") (message "2"))
;; (and
;;  (progn
;;    (message "1")
;;    nil)
;;  (progn
;;    (message "2")
;;    nil))

(defun factorial-naïve (n)
  "N."
  (cond ((= n 0) 1)
	((= n 1) 1)
	((* n (factorial-naïve (- n 1))))))

(defun factorial-tc (n)
  "N."
  (defun tc (acc x)
    "ITER X."
    (cond ((= x 1) acc)
	  ((tc (* acc x) (- x 1)))))
  (cond ((= n 0) 1)
	((= n 1) 1)
	((tc 1 n))))

(defun factorial-tco (n)
  "N."
  (defun bounce (acc x)
    "ACC X."
    (cond ((= x 0) acc)
	  ((= x 1) acc)
	  ((lambda () (bounce (* acc x) (- x 1))))))
  (let ((res (bounce 1 n)))
    (while (functionp res)
      (setq res (funcall res)))
    res))

(defvar s '(0 1 2 3 4 5 6 7 8 9))
(defvar r-fact '(1 1 2 6 24 120 720 5040 40320 362880))
(defvar r-fibs '(0 1 1 2 3 5 8 13 21 34))

(ert-deftest factorial-tests ()
  (should (equal (-map #'factorial-naïve s) r-fact))
  (should (equal (-map #'factorial-tc s) r-fact))
  (should (equal (-map #'factorial-tco s) r-fact)))

;; (-map #'factorial-tc (-map (lambda (x) (* x x x)) s))
;; (message "%s" (-map #'factorial-tco s))

(defun fibonacci-naïve (n)
  "N."
  (cond ((= n 0) 0)
	((= n 1) 1)
	((+ (fibonacci-naïve (- n 1))
	    (fibonacci-naïve (- n 2))))))

(defun fibonacci-tc (n)
  "N."
  (defun tc (nm1 nm2 x)
    "NM1 NM2 X."
    (cond ((= x n) (+ nm1 nm2))
	  ((tc (+ nm1 nm2) nm1 (+ x 1)))))
  (cond ((= n 0) 0)
	((= n 1) 1)
	((tc 1 0 2))))

(defun fibonacci-tco (n)
  "N."
  (defun bounce (nm1 nm2 x)
    "NM1 NM2 X."
    (cond ((= x n) (+ nm1 nm2))
	  ((lambda () (bounce (+ nm1 nm2) nm1 (+ x 1))))))
  (cond ((= n 0) 0)
	((= n 1) 1)
	((let ((res (bounce 1 0 2)))
	   (while (functionp res)
	     (setq res (funcall res)))
	   res))))

(ert-deftest fibonacci-tests ()
  (should (equal (-map #'fibonacci-naïve s) r-fibs))
  (should (equal (-map #'fibonacci-tc s) r-fibs))
  (should (equal (-map #'fibonacci-tco s) r-fibs)))

;; (message "%s" (-map #'fibonacci-naïve s))
;; (message "%s" (-map #'fibonacci-tc s))
;; (message "%s" (-map #'fibonacci-tco s))

;; Make change
(defun cc (denoms amount)
  "DENOMS AMOUNT."
  (cond ((= amount 0) 1)
	((or (null denoms) (< amount 0)) 0)
	((+ (cc (cdr denoms) amount)
	    (cc denoms (- amount (car denoms)))))))

(cc '(1 5 10 25 50) 200)

(defun ck (denoms amount k)
  "DENOMS AMOUNT K."
  (cond ((= amount 0) (funcall k 1))
	((or (< amount 0) (null denoms)) (funcall k 0))
	((let ((nk (lambda (r) (ck denoms (- amount (car denoms)) (lambda (s) (funcall k (+ r s)))))))
	   (ck (cdr denoms) amount nk)))))

(defun ckt (denoms amount)
  "DENOMS AMOUNT K."
  (defun bounce (denoms amount k)
    "K."
    (cond ((= amount 0) (lambda () (funcall k 1)))
	  ((or (< amount 0) (null denoms)) (lambda () (funcall k 0)))
	  ((let ((nk (lambda (r) (lambda () (bounce denoms (- amount (car denoms)) (lambda (s) (lambda () (funcall k (+ r s)))))))))
	     (lambda () (bounce (cdr denoms) amount nk))))))
  (let ((res (bounce denoms amount (lambda (r) r))))
    (while (functionp res)
      (setq res (funcall res)))
    res))

(ckt '(1 5 10 25 50 100) 300)

?a
;;; expressions.el ends here

