;;; package --- Chapter#2: Data Abstractions -*- lexical-binding: t; -*-
;;; Commentary:
;;; Second chapter of SICP deals with notion of compound data and
;;; separation of data representation and data use.
(require 'dash)
(require 'dash-functional)
(require 'ert)

;; define error hierarchy for arithmetic operations
(define-error 'arith "Arithmetic operation error")
(define-error 'rat "Rational number arithmetic error" 'arith)

;; rational number arithmetic interface
(defun make-rat (n d)
  "N D."
  `("rational" ,n ,d))

(defun ratp (r)
  "R."
  (if (and (listp r)
	   (= (length r) 3)
	   (string= (car r) "rational")
	   (numberp (cadr r))
	   (numberp (caddr r)))
      t
    nil))

(defun numer (r)
  "R."
  (if (not (ratp r))
      (signal 'rat `(,r))
    (cadr r)))

(defun denom (r)
  "R."
  (if (not (ratp r))
      (signal 'rat `(,r))
    (caddr r)))

;; (setq n0 (make-rat 2 3))
;; (cadr n0)
;; (caddr n0)
;; (length n0)
;; (car n0)
;; (equal (car (list "Prateem")) "Prateem")
;; (numer n0)
;; (denom n0)
;; ;; (denom `("rational" 2))

(defun fmap (f xs)
  "F XS."
  (defun bounce (s k)
    (cond ((null s) (lambda () (funcall k s)))
	  ((let ((hd (car s))
		 (tl (cdr s)))
	     (lambda () (bounce tl (lambda (l) (lambda () (funcall k (cons (funcall f hd) l))))))))))
  (let ((res (bounce xs (lambda (x) x))))
    (while (functionp res)
      (setq res (funcall res)))
    res))

(fmap (lambda (x) (* x x)) (number-sequence 0 20000))

;;; Code:
;;; dataabstractions.el ends here
