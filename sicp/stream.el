;;; package --- Stream Library -*- lexical-binding: t; -*-
;;; Code:
;;; Commentary:
(defun foo () "." (interactive) (message "Hello, World!"))
(defun id (x) "X." x)
;;; stream.el ends here
