;;; package --- SICP Chapter#3: Modularity, Object and State -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(defun make-withdraw ()
  "Withdraw object with `balance' state internal to the object."
  (let ((balance 100))
    (lambda (amount)
      (setq balance (- balance amount))
      (message "%d" balance))))

(setq b0 (make-withdraw))
(setq b1 (make-withdraw))
(funcall b0 10)
(funcall b1 5)

(defun square (x) "X." (* x x))

((lambda (x y) (+ x y)) 1 2)



;;; state.el ends here
