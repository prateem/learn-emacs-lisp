;;; trampoline.el --- Trampoline implementation -*- lexical-binding: t -*-
;;; Commentary:
;;; Will demo trampolines, tco and cps.
;;; Code:

(require 'dash)
(require 'dash-functional)
(require 'comp)
(require 'cl-lib)
(require 'cl-generic)

;;; Trampoline, TCO, Event Loop, Async, CPS and so on down the rabbit hole
(defun factorial-näive (n)
  "Factorial of N."
  (if (= n 0)
      1
    (* n (factorial-näive (- n 1)))))

;; (comp-spill-lap #'factorial-näive)

(defun -& (x y k) (funcall k (calc-eval (format "%s - %s" x y))))
(defun *& (x y k) (funcall k (calc-eval (format "%s * %s" x y))))
(defun factorial& (n k)
  "Factorial of N K."
  (if (string= n "0")
      (funcall k "1")
    (-& n "1" (lambda (nm1)
	      (factorial& nm1 (lambda (fact)
				(*& n fact k)))))))
(fset 'factorial-k
      (-partial (-flip #'factorial&)
		(lambda (x) x)))

(defun -&< (x y k) (lambda () (funcall k (calc-eval (format "%s - %s" x y)))))
(defun *&< (x y k) (lambda () (funcall k (calc-eval (format "%s * %s" x y)))))
(defun factorial&< (n k)
  "Factorial of N K."
  (if (string= n "0")
      (lambda () (funcall k "1"))
    (-&< n "1" (lambda (nm1)
	       (factorial&< nm1 (lambda (fact)
				  (*&< n fact k)))))))
(defun factorial-t (n)
  "Factorial of N."
  (let ((ret (factorial&< n (lambda (x) x))))
    (while (functionp ret)
      (setq ret (funcall ret)))
    ret))

(factorial-t "10")

(defun factorial-tco (n)
  "Factorial of N."
  (defun fact-loop (a i)
    "Loop A I."
    (if (> i n)
	a
      (fact-loop (* a i) (+ i 1))))
  (if (= n 0)
      1
    (fact-loop 1 1)))

;; (-map #'factorial-näive '(0 1 2 3 4 5 6 7 8 9))
;; (-map #'factorial-tco '(0 1 2 3 4 5 6 7 8 9))
;; (-map #'factorial-k (-map #'number-to-string '(0 1 2 3 4 5 6 7 8 9)))
;; (-map #'factorial-t (-map #'number-to-string '(0 1 2 3 4 5 6 7 8 9)))
;; ;; this won't work as all nested lambdas will get created on the heap
;; ;; first and at the end when executed it will overflow the stack.
;; (factorial-k "1000")
;; ;; this will work as all nested lambdas are interspersed with dummy
;; ;; lambdas that then gets trampolined.
(factorial-t "1000")

(ert-deftest factorial-test ()
  (should (equal (-map #'factorial-näive '(0 1 2 3 4 5 6 7 8 9 10 11))
		 '(1 1 2 6 24 120 720 5040 40320 362880 3628800 39916800)))
  (should (equal (-map #'factorial-tco '(0 1 2 3 4 5 6 7 8 9 10 11))
		 '(1 1 2 6 24 120 720 5040 40320 362880 3628800 39916800)))
  (should (equal (-map #'factorial-k (-map #'number-to-string '(0 1 2 3 4 5 6 7 8 9 10 11)))
		 (-map #'number-to-string '(1 1 2 6 24 120 720 5040 40320 362880 3628800 39916800))))
  (should (equal (-map #'factorial-t (-map #'number-to-string '(0 1 2 3 4 5 6 7 8 9 10 11)))
		 (-map #'number-to-string '(1 1 2 6 24 120 720 5040 40320 362880 3628800 39916800)))))

;;; Non local exit semantics and error handling
;; catch & throw
(defun foo-outer (a)
  "A."
  (catch 'foo
    (foo-inner a)))

(defun foo-inner (a)
  "A."
  (if a
      (progn
	(message "normal execution")
	t)
    (throw 'foo
	   (progn
	     (message "throw called")
	     nil))))

(foo-outer nil)

;; Testing nominal use of
;; 1. condition-case usage
;; 2. error symbol hierarchy
;; 3. scope of local variables
;;    - esp access to local variables within error handlers
;; 4. interaction between condition-case and unwind-protect
(setq l123 '(1 2 3))
(define-error 'app-error "Application error")
(define-error 'auth-error "Authentication error" 'app-error)
;; (signal 'auth-error `("Authentication to Zoho.com failed" ,(+ 1 2)))
(let ((lvar0 10)
      (lvar1 11))
  (condition-case err
      (let ((lvar2 12)
	    (lvar3 13))
	(message "Entering protected body")
	(signal 'auth-error `("Wrong password" ,@l123)))
    (auth-error
     (princ (format "%s %d" err lvar0)))
    (app-error
     (princ (format "%s" err)))))

(condition-case err
    (let ((lv0 0)
	  (lv1 1))
      (unwind-protect
	  (progn
	    (message "Body form")
	    (signal 'auth-error `("Authentication error" ,(+ 2 3))))
	(message "Unwind form line#1")
	(message "Unwind form line#2")))
  (app-error
   (message "Application error handler")
   (princ (format "%s" err)))
  (auth-error
   (message "Authentication error handler")
   (princ (format "%s" err))))

;; event loop
(defun foo (k)
  "K."
  (message (format "f: %s\nv: %s"
		   (symbol-function 'k)
		   (symbol-function 'k)))
  (lambda () (funcall k k)))

(defun bar (k)
  "K."
  (message "bar")
  (lambda () (funcall k k)))

;; (let ((k (foo #'foo)))
;;   (while (functionp k)
;;     (sleep-for 1)
;;     (setq k (funcall k))))

;;; explore dolist
(let ((sum 0)
      (count 0.0))
  (dolist (elem '(20 10 0) (/ sum count))
    (setf sum (+ sum elem) count (1+ count))))

;;; purely functional struct
(defun employee (id name dept)
  "Make employee structure with slots ID, NAME and DEPT."
  (lambda (f) (funcall f id name dept)))

(defun employee-get-id (e)
  "Get ID from E."
  (funcall e (lambda (id name dept) id)))

(defun employee-get-name (e)
  "Get NAME from E."
  (funcall e (lambda (id name dept) name)))

(defun employee-get-dept (e)
  "Get DEPT from E."
  (funcall e (lambda (id name dept) dept)))

(defun employee-set-id (e id)
  "Set ID in E."
  (let ((name (employee-get-name e))
	(dept (employee-get-dept e)))
    (employee id name dept)))

(defun employee-set-name (e name)
  "Set NAME in E."
  (let ((id (employee-get-id e))
	(dept (employee-get-dept e)))
    (employee id name dept)))

(defun employee-set-dept (e dept)
  "Set DEPT in E."
  (let ((id (employee-get-id e))
	(name (employee-get-name e)))
    (employee id name dept)))

(setq emp (employee "0010234702" "Prateem Mandal" "Engineering"))
(message (format "name: %s\ndept: %s" (employee-get-name emp) (employee-get-dept emp)))
(setq emp (employee-set-dept emp "Sales"))
(message (format "name: %s\ndept: %s" (employee-get-name emp) (employee-get-dept emp)))

;;; investigating quote semantics
;; it is possible to call this way but frowned upon
;; right way is to add a funcall in the front
((lambda (x) x) 4)

;; here both are arguments are evaluated from left
;; to right and then the function body is executed
(funcall (progn
	   (message "function")
	   (lambda (x) (progn
			 (message "lambda")
			 x)))
	 (progn
	   (message "param")
	   4))

;; this will not execute, first element of list
;; is _not_ evaluated, remember?
;; ((progn
;;    (message "function")
;;    (lambda (x) x))
;;  (progn
;;    (message "param")
;;    4))

;; boo is a function, when the function symbol
;; boo is passed to boo-outer, what does param
;; f mean? f should hold as value the "symbol
;; boo"? in that case (symbol-function f) should
;; mean (symbol-function 'boo) and should print
;; the function definition
(defun boo (x) (+ 1 x))
(defun boo-outer (f) (symbol-function f))
(boo-outer 'boo)
(symbol-function 'boo)

;; these two examples show the difference between
;; symbol containing the function as function vs
;; function as value. both could be used for
;; invoking the function though in different ways
(fset 'a (read "(lambda (x) x)"))
(funcall #'a 4)
(symbol-function 'a)

(setq b (read "(lambda (x) x)"))
(funcall b 4)
(symbol-value 'b)

;; here the expectation is that v is bound to value
;; 10 and therefore symbol-value of v will return 10
;; this does not happen and a void-variable v error
;; is returned, why? turnsout in lexical scoping
;; local variables are stripped in actual execution
;; (let ((v 10))
;;   (symbol-value 'v))

;;; ligatures
;; does ligatures with fira code work? it does in
;; terminal mode on patched st, but gui mode needs
;; additional cofigurations in init file
(<= 2 3)
(>= 2 3)

;; dolist once again
(setq res
      (let ((sum 0)
	    (count 0.0))
	(dolist (i '(1 2 3) (/ sum count))
	  (message (format "%d" i))
	  (setq sum (+ i sum))
	  (setq count (1+ count)))))

;;; DataType, Product, Class and AdHoc Polymorphism
;; https://nullprogram.com/blog/2018/02/14/
;; create the structure type
(cl-defstruct (person (:constructor person--create)
		      (:copier nil))
  (id :read-only)
  name
  dept
  ts)

;; create private constructor
(cl-defun person-create (&rest args)
  "Constructor autofilling timestamp and passing rest ARGS to private constructor."
  (apply #'person--create :ts (float-time) args))

;; make an object
(setq
 prateem
 (person-create
  :id "0102203"
  :name "Prateem Mandal"
  :dept "Engineering"))

;; test object type
(person-p prateem)

;; access fields of the object
(princ (format "%s" prateem))
(princ (format "Name: %s\nDept: %s\nRecord Created: %s"
	       (person-name prateem)
	       (person-dept prateem)
	       (person-ts prateem)))

;; mutate fields of the object
(setf (person-dept prateem) "Sales")

;; Example of hand rolled dynamic dispatch
;; class/interface 'shape' shows two
;; capabilities 'volume' and 'area'
(cl-defstruct (shape (:constructor shape--create)
		     (:copier nil))
  volume-fn
  area-fn)
;; generic functions volume and area
(cl-defun volume (obj) "OBJ." (funcall (shape-volume-fn obj) obj))
(cl-defun area (obj) "OBJ." (funcall (shape-area-fn obj) obj))

;; datatype 'box' is 'shape' like
(cl-defstruct (box (:include shape)
		   (:constructor box--create)
		   (:copier nil))
  length
  breadth
  height)
;; public constructor of 'box'
(cl-defun box-create (&rest args)
  "ARGS."
  (apply #'box--create
	 :volume-fn
	 (lambda (obj)
	   (let ((l (box-length obj))
		 (b (box-breadth obj))
		 (h (box-height obj)))
	     (* l b h)))
	 :area-fn
	 (lambda (obj)
	   (let ((l (box-length obj))
		 (b (box-breadth obj))
		 (h (box-height obj)))
	     (* 2 (+ (* l b)
		     (* b h)
		     (* h l)))))
	 args))

;; datatype 'sphere' is 'shape' like
(cl-defstruct (sphere (:include shape)
		      (:constructor sphere--create))
  radius)
;; public constructor of 'sphere'
(defconst π 3.1415)
(cl-defun sphere-create (&rest args)
  "ARGS."
  (apply #'sphere--create
	 :volume-fn
	 (lambda (obj)
	   (let ((r (sphere-radius obj)))
	     (* (/ 4.0 3.0) π r r r)))
	 :area-fn
	 (lambda (obj)
	   (let ((r (sphere-radius obj)))
	     (* 4 π r r)))
	 args))

;; demo function dispatch on object type
(setq sh0 (sphere-create :radius 22.0))
(princ
 (format "Sphere's volume: %f\nSphere's surface area: %f"
	 (volume sh0) (area sh0)))
(setq sh1 (box-create :length 1.0 :breadth 1.0 :height 1.0))
(princ
 (format "Box's volume: %f\nBox's surface area: %f"
	 (volume sh1) (area sh1)))

;; generic function through cl-generic
(cl-defgeneric genvolume (shape) "SHAPE.")
(cl-defmethod genvolume ((obj box))
  "OBJ."
  (let ((l (box-length obj))
	(b (box-breadth obj))
	(h (box-height obj)))
    (* l b h)))
(cl-defmethod genvolume ((obj sphere))
  "OBJ."
  (let ((r (sphere-radius obj)))
    (* (/ 4.0 3.0) π r r r)))

(cl-defgeneric genarea (shape) "SHAPE.")
(cl-defmethod genarea ((obj box))
  "OBJ."
  (let ((l (box-length obj))
	(b (box-breadth obj))
	(h (box-height obj)))
    (* 2 (+ (* l b)
	    (* b h)
	    (* h l)))))
(cl-defmethod genarea ((obj sphere))
  "OBJ."
  (let ((r (sphere-radius obj)))
    (* 4 π r r)))

;; demo function dispatch on object type
(princ
 (format "Sphere's volume: %f\nSphere's surface area: %f"
	 (volume sh0) (area sh0)))
(princ
 (format "Box's volume: %f\nBox's surface area: %f"
	 (volume sh1) (area sh1)))
(princ
 (format "Sphere's volume: %f\nSphere's surface area: %f"
	 (genvolume sh0) (genarea sh0)))
(princ
 (format "Box's volume: %f\nBox's surface area: %f"
	 (genvolume sh1) (genarea sh1)))

;;; generators
;;; standalone batch mode programs
;;; benchmarking
;;; macros
;;; programmatic cps transformation
(provide 'trampoline)
;;; trampoline.el ends here
